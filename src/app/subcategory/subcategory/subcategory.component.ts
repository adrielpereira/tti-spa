import { Component, OnInit } from '@angular/core';
import { Subcategory } from 'src/app/_models/subcategory';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { CategoryService } from 'src/app/_services/category.service';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { Router } from '@angular/router';
import { SubcategoryService } from 'src/app/_services/subcategory.service';
import { Category } from 'src/app/_models/category';

@Component({
  selector: 'app-subcategory',
  templateUrl: './subcategory.component.html',
  styleUrls: ['./subcategory.component.css']
})
export class SubcategoryComponent implements OnInit {
  subcategoryForm: FormGroup;
  subcategory: Subcategory;
  categories: Category[];

  constructor(private categoryService: CategoryService, private subcategoryService: SubcategoryService,
    private alertify: AlertifyService, private formBuilder: FormBuilder, private route: Router) { }

  ngOnInit() {
    this.subcategoryForm = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(150)]),
      categoryId: new FormControl('', [Validators.required]),
    });
    this.getCategory();
  }

  get f() { return this.subcategoryForm.controls; }

  onSubmit() {
    if (this.subcategoryForm.valid) {
      this.subcategory = Object.assign({}, this.subcategoryForm.value);
      this.subcategoryService.postSubcategory(this.subcategory).subscribe( () => {
        this.alertify.success('Product save successfully');
        this.route.navigate(['/subcategory']);
      }, error => {
        this.alertify.error(error);
      });
    }
  }

  getCategory() {
    this.categoryService.getCategories().subscribe( response => {
      this.categories = response;
    }, error => {
      console.log(error);
    });
  }
  cancelAddSubcategory() {
    this.route.navigate(['/subcategory']);
  }
}
