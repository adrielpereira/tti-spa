import { Component, OnInit } from '@angular/core';
import { ProductService } from '../_services/product.service';
import { Product } from '../_models/product';
import { AlertifyService } from '../_services/alertify.service';
import { ActivatedRoute } from '@angular/router';
import { Pagination, PaginatedResult } from '../_models/pagination';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  divProduct = false;
  products: Product[];
  pagination: Pagination;
  userParams: any = {};

  constructor(private productService: ProductService, private alertify: AlertifyService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.products = data['products'].result;
      this.pagination = data['products'].pagination;
    });

    this.userParams.orderBy = 'id';
    this.userParams.order = 'ASC';
  }

  pageChanged(event: any): void {
    this.pagination.currentPage = event.page;
    this.loadProducts();
  }

  loadProducts() {
    this.productService.getProducts(this.pagination.currentPage, this.pagination.itemsPerPage, this.userParams)
    .subscribe((res: PaginatedResult<Product[]>) => {
      this.products = res.result;
      this.pagination = res.pagination;
    }, error => {
      this.alertify.error(error);
    });
  }

  addProduct() {
    this.divProduct = !this.divProduct;
  }

  cancelAddProduct(cancelMode: boolean) {
    this.divProduct = cancelMode;
  }


  order(label) {
    if (label === this.userParams.orderBy) {
      this.userParams.order = this.userParams.order === 'ASC' ? 'DESC' : 'ASC';
    } else {
      this.userParams.order = 'ASC';
      this.userParams.orderBy = label;
    }
    this.loadProducts();
  }

}
