import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faArrowCircleUp, faArrowCircleDown, faCoffee } from '@fortawesome/free-solid-svg-icons';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { ProductService } from './_services/product.service';
import { HomeComponent } from './home/home.component';
import { AlertifyService } from './_services/alertify.service';
import { CategoryListComponent } from './category/category-list/category-list.component';
import { SubcategoryListComponent } from './subcategory/subcategory-list/subcategory-list.component';
import { ProductComponent } from './product/product/product.component';
import { ProductDetailComponent } from './product/product-detail/product-detail.component';
import { ProductCardComponent } from './product/product-card/product-card.component';
import { ProductEditComponent } from './product/product-edit/product-edit.component';
import { ProductDetailResolver } from './_resolvers/product-detail.resolver';
import { ProductEditResolver } from './_resolvers/product-edit.resolver';
import { CategoryListResolver } from './_resolvers/category-list.resolver';
import { ProductDeleteComponent } from './product/product-delete/product-delete.component';
import { ProductDeleteResolver } from './_resolvers/product-delete.resolver';
import { CategoryEditComponent } from './category/category-edit/category-edit.component';
import { CategoryEditResolver } from './_resolvers/category-edit.resolver';
import { CategoryDeleteComponent } from './category/category-delete/category-delete.component';
import { CategoryDeleteResolver } from './_resolvers/category-delete.resolver';
import { SubcategoryService } from './_services/subcategory.service';
import { SubcategoryListResolver } from './_resolvers/subcategory-list.resolver';
import { SubcategoryDeleteResolver } from './_resolvers/subcategory-delete.resolver';
import { SubcategoryEditResolver } from './_resolvers/subcategory-edit.resolver';
import { SubcategoryDeleteComponent } from './subcategory/subcategory-delete/subcategory-delete.component';
import { SubcategoryEditComponent } from './subcategory/subcategory-edit/subcategory-edit.component';
import { CategoryComponent } from './category/category/category.component';
import { SubcategoryComponent } from './subcategory/subcategory/subcategory.component';
import { ProductListResolver } from './_resolvers/product-list.resolver';

@NgModule({
   declarations: [
      AppComponent,
      NavComponent,
      HomeComponent,
      CategoryListComponent,
      SubcategoryListComponent,
      ProductComponent,
      ProductDetailComponent,
      ProductCardComponent,
      ProductEditComponent,
      ProductDeleteComponent,
      CategoryEditComponent,
      CategoryDeleteComponent,
      SubcategoryDeleteComponent,
      SubcategoryEditComponent,
      CategoryComponent,
      SubcategoryComponent
   ],
   imports: [
      BrowserModule,
      AppRoutingModule,
      HttpClientModule,
      FormsModule,
      ReactiveFormsModule,
      PaginationModule.forRoot(),
      FontAwesomeModule
   ],
   providers: [
      ProductService,
      AlertifyService,
      ProductDetailResolver,
      ProductEditResolver,
      CategoryListResolver,
      ProductDeleteResolver,
      CategoryEditResolver,
      CategoryDeleteResolver,
      SubcategoryService,
      SubcategoryListResolver,
      SubcategoryDeleteResolver,
      SubcategoryEditResolver,
      ProductListResolver
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule {
   constructor() {
      library.add(faArrowCircleUp);
      library.add(faArrowCircleDown);
      library.add(faCoffee);
   }

}
