import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ProductComponent } from './product/product/product.component';
import { CategoryListComponent } from './category/category-list/category-list.component';
import { SubcategoryListComponent } from './subcategory/subcategory-list/subcategory-list.component';
import { ProductDetailComponent } from './product/product-detail/product-detail.component';
import { ProductEditComponent } from './product/product-edit/product-edit.component';
import { ProductDetailResolver } from './_resolvers/product-detail.resolver';
import { ProductEditResolver } from './_resolvers/product-edit.resolver';
import { CategoryListResolver } from './_resolvers/category-list.resolver';
import { ProductDeleteComponent } from './product/product-delete/product-delete.component';
import { ProductDeleteResolver } from './_resolvers/product-delete.resolver';
import { CategoryEditComponent } from './category/category-edit/category-edit.component';
import { CategoryEditResolver } from './_resolvers/category-edit.resolver';
import { CategoryDeleteComponent } from './category/category-delete/category-delete.component';
import { CategoryDeleteResolver } from './_resolvers/category-delete.resolver';
import { SubcategoryEditComponent } from './subcategory/subcategory-edit/subcategory-edit.component';
import { SubcategoryDeleteComponent } from './subcategory/subcategory-delete/subcategory-delete.component';
import { SubcategoryListResolver } from './_resolvers/subcategory-list.resolver';
import { SubcategoryEditResolver } from './_resolvers/subcategory-edit.resolver';
import { SubcategoryDeleteResolver } from './_resolvers/subcategory-delete.resolver';
import { CategoryComponent } from './category/category/category.component';
import { SubcategoryComponent } from './subcategory/subcategory/subcategory.component';
import { ProductListResolver } from './_resolvers/product-list.resolver';

const routes: Routes = [
  {path: 'home', component: HomeComponent, resolve: {products: ProductListResolver}},
  {path: 'product', component: ProductComponent},
  {path: 'product/:id', component: ProductDetailComponent, resolve: { product: ProductDetailResolver}},
  {path: 'product/edit/:id', component: ProductEditComponent, resolve: { product: ProductEditResolver}},
  {path: 'product/delete/:id', component: ProductDeleteComponent, resolve: { product: ProductDeleteResolver}},
  {path: 'category', component: CategoryListComponent, resolve: {categories: CategoryListResolver}},
  {path: 'category/add', component: CategoryComponent},
  {path: 'category/edit/:id', component: CategoryEditComponent, resolve: { category: CategoryEditResolver}},
  {path: 'category/delete/:id', component: CategoryDeleteComponent, resolve: { category: CategoryDeleteResolver}},
  {path: 'subcategory', component: SubcategoryListComponent, resolve: {subcategories: SubcategoryListResolver}},
  {path: 'subcategory/add', component: SubcategoryComponent},
  {path: 'subcategory/edit/:id', component: SubcategoryEditComponent, resolve: { subcategory: SubcategoryEditResolver}},
  {path: 'subcategory/delete/:id', component: SubcategoryDeleteComponent, resolve: { subcategory: SubcategoryDeleteResolver}},
  {path: '**', redirectTo: 'home', pathMatch: 'full'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
