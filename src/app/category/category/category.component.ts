import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Category } from 'src/app/_models/category';
import { CategoryService } from 'src/app/_services/category.service';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  categoryForm: FormGroup;
  category: Category;

  constructor(private categoryService: CategoryService, private alertify: AlertifyService,
     private formBuilder: FormBuilder, private route: Router) { }

  ngOnInit() {
    this.categoryForm = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(150)]),
    });
  }

  goBack() {
    this.route.navigate(['/category']);
  }

  get f() { return this.categoryForm.controls; }

  onSubmit() {
    if (this.categoryForm.valid) {
      this.category = Object.assign({}, this.categoryForm.value);
      this.categoryService.postCategory(this.category).subscribe( () => {
        this.alertify.success('Product save successfully');
        this.route.navigate(['/category']);
      }, error => {
        this.alertify.error(error);
      });
    }
  }

}
