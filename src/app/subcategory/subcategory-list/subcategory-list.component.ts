import { Component, OnInit } from '@angular/core';
import { SubcategoryService } from 'src/app/_services/subcategory.service';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { Subcategory } from 'src/app/_models/subcategory';

@Component({
  selector: 'app-subcategory-list',
  templateUrl: './subcategory-list.component.html',
  styleUrls: ['./subcategory-list.component.css']
})
export class SubcategoryListComponent implements OnInit {
  subcategories: Subcategory[];

  constructor(private subcategoryService: SubcategoryService, private alertify: AlertifyService) { }

  ngOnInit() {
    this.loadCategories();
  }
  loadCategories() {
    this.subcategoryService.getSubcategories().subscribe( (subcategories: Subcategory[]) => {
      this.subcategories = subcategories;
    }, error => {
      this.alertify.error(error);
    });
  }

}
