import { Photo } from './photo';
import { Subcategory } from './subcategory';
import { Category } from './category';

export interface Product {
    id: number;
    name: string;
    detail: string;
    categoryId: number;
    price: number;
    category: Category;
    photos?: Photo[];
    subcategories?: Subcategory[];
}
