import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/_models/product';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { ProductService } from 'src/app/_services/product.service';

@Component({
  selector: 'app-product-delete',
  templateUrl: './product-delete.component.html',
  styleUrls: ['./product-delete.component.css']
})
export class ProductDeleteComponent implements OnInit {
  product: Product;

  constructor(private route: ActivatedRoute, private router: Router,
     private alertify: AlertifyService, private productService: ProductService) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.product = data['product'];
    });
  }

  deleteItem(id: number) {
    this.productService.deleteProduct(this.product.id).subscribe( next => {
      this.alertify.success('Product deleted successfully');
      this.router.navigate(['/products']);
    }, error => {
       this.alertify.error('Problem to delete your product');
    });
  }

  goBack() {
    this.router.navigate(['/products']);
  }
}
