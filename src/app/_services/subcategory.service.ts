import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Subcategory } from '../_models/subcategory';

@Injectable({
  providedIn: 'root'
})
export class SubcategoryService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getSubcategories(): Observable<Subcategory[]> {
    return  this.http.get<Subcategory[]>(this.baseUrl + 'subcategories/');
  }

  getSubcategory(id): Observable<Subcategory> {
    return  this.http.get<Subcategory>(this.baseUrl + 'subcategories/' + id);
  }

  postSubcategory(subcategory: Subcategory) {
    console.log(subcategory);
    return  this.http.post(this.baseUrl + 'subcategories/', subcategory);
  }

  putSubcategory(id: number, subcategory: Subcategory) {
    return  this.http.put(this.baseUrl + 'subcategories/' + id , subcategory);
  }

  deleteCSubcategory(id: number) {
    return  this.http.delete(this.baseUrl + 'subcategories/' + id);
  }
}
