import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subcategory } from 'src/app/_models/subcategory';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { SubcategoryService } from 'src/app/_services/subcategory.service';

@Component({
  selector: 'app-subcategory-delete',
  templateUrl: './subcategory-delete.component.html',
  styleUrls: ['./subcategory-delete.component.css']
})
export class SubcategoryDeleteComponent implements OnInit {
  subcategory: Subcategory;
  constructor(private route: ActivatedRoute, private router: Router,
    private alertify: AlertifyService, private subcategoryService: SubcategoryService) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.subcategory = data['subcategory'];
    });
  }

  deleteItem(id: number) {
    this.subcategoryService.deleteCSubcategory(this.subcategory.id).subscribe( next => {
      this.alertify.success('Subcategory deleted successfully');
      this.router.navigate(['/subcategory']);
    }, error => {
       this.alertify.error(error);
    });
  }

  goBack() {
    this.router.navigate(['/subcategory']);
  }

}
