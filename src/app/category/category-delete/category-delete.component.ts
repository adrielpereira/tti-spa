import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/_models/category';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { CategoryService } from 'src/app/_services/category.service';

@Component({
  selector: 'app-category-delete',
  templateUrl: './category-delete.component.html',
  styleUrls: ['./category-delete.component.css']
})
export class CategoryDeleteComponent implements OnInit {
  category: Category;
  constructor(private route: ActivatedRoute, private router: Router,
    private alertify: AlertifyService, private categoryService: CategoryService) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.category = data['category'];
    });
  }

  deleteItem(id: number) {
    this.categoryService.deleteCategory(this.category.id).subscribe( next => {
      this.alertify.success('Category deleted successfully');
      this.router.navigate(['/category']);
    }, error => {
       this.alertify.error(error);
    });
  }

  goBack() {
    this.router.navigate(['/category']);
  }

}
