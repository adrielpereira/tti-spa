import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subcategory } from 'src/app/_models/subcategory';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { SubcategoryService } from 'src/app/_services/subcategory.service';
import { Category } from 'src/app/_models/category';
import { CategoryService } from 'src/app/_services/category.service';

@Component({
  selector: 'app-subcategory-edit',
  templateUrl: './subcategory-edit.component.html',
  styleUrls: ['./subcategory-edit.component.css']
})
export class SubcategoryEditComponent implements OnInit {
  @ViewChild('editForm') editForm: NgForm;
  subcategory: Subcategory;
  categories: Category[];

  constructor(private route: ActivatedRoute, private subcategoryService: SubcategoryService,
    private categoryServices: CategoryService, private alertify: AlertifyService, private router: Router) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.subcategory = data['subcategory'];
    });
    this.loadCategories();
  }

  updateSubcategory() {
    this.subcategoryService.putSubcategory(this.subcategory.id, this.subcategory).subscribe( next => {
      this.alertify.success('Cateogry update successfully');
      this.editForm.reset(this.subcategory);
      this.router.navigate(['/subcategory']);
    }, error => {
       this.alertify.error('Problem to update your product');
    });
  }

  loadCategories() {
    this.categoryServices.getCategories().subscribe((categories: Category[]) => {
      this.categories = categories;
    }, error => {
      this.alertify.error(error);
    });
  }

  goBack() {
    this.router.navigate(['/subcategory']);
  }

}
