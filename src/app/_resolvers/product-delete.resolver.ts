import { Injectable } from '@angular/core';
import { Product } from '../_models/product';
import { ProductService } from '../_services/product.service';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { AlertifyService } from '../_services/alertify.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';


@Injectable()
export class ProductDeleteResolver implements Resolve<Product> {
    constructor( private productService: ProductService, private route: Router, private alertify: AlertifyService) {}

    resolve(route: ActivatedRouteSnapshot): Observable<Product> {
        return this.productService.getProduct(route.params['id']).pipe(
            catchError(error => {
                this.alertify.error(error);
                this.route.navigate(['/products']);
                return of(null);
            })
        );
    }
}
