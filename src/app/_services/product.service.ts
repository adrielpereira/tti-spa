import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { Product } from '../_models/product';
import { PaginatedResult } from '../_models/pagination';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  products: any;
  baseUrl = environment.apiUrl ;

constructor(private http: HttpClient) { }
  model: any = {};

  getProducts(page?, itemsPerPage?, userParams?): Observable<PaginatedResult<Product[]>> {

    const paginatedResult: PaginatedResult<Product[]> = new PaginatedResult<Product[]>();

    let params = new HttpParams();

    if (page != null && itemsPerPage != null) {
      params = params.append('pageNumber', page);
      params = params.append('pageSize', itemsPerPage);
    }

    if (userParams != null && userParams.search != null) {
      params = params.append('search', userParams.search);
    }

    if (userParams != null && userParams.orderBy != null) {
      params = params.append('orderBy', userParams.orderBy);
    }

    if (userParams != null && userParams.order != null) {
      params = params.append('order', userParams.order);
    }

    return  this.http.get<Product[]>(this.baseUrl + 'products/', { observe: 'response', params}).pipe(
      map(response => {
        paginatedResult.result = response.body;
        if (response.headers.get('Pagination') != null) {
        paginatedResult.pagination = JSON.parse(response.headers.get('Pagination'))
        }
        return paginatedResult;
      })
    );
  }

  getProduct(id): Observable<Product> {
    return  this.http.get<Product>(this.baseUrl + 'products/' + id);
  }

  postProduct(product: Product) {
    console.log(product);
    return  this.http.post(this.baseUrl + 'products/', product);
  }

  putProduct(id: number, product: Product) {
    return  this.http.put(this.baseUrl + 'products/' + id , product);
  }

  deleteProduct(id: number) {
    console.log(this.baseUrl + 'products/' + id);
    return  this.http.delete(this.baseUrl + 'products/' + id);
  }

}
