import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Category } from '../_models/category';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  baseUrl = environment.apiUrl;

constructor(private http: HttpClient) { }

getCategories(): Observable<Category[]> {
  return  this.http.get<Category[]>(this.baseUrl + 'categories/');
}

getCategory(id): Observable<Category> {
  return  this.http.get<Category>(this.baseUrl + 'categories/' + id);
}

postCategory(category: Category) {
  console.log(category);
  return  this.http.post(this.baseUrl + 'categories/', category);
}

putCategory(id: number, category: Category) {
  return  this.http.put(this.baseUrl + 'categories/' + id , category);
}

deleteCategory(id: number) {
  console.log(this.baseUrl + 'categories/' + id);
  return  this.http.delete(this.baseUrl + 'categories/' + id);
}
}
