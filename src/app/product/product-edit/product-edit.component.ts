import { Component, OnInit, ViewChild } from '@angular/core';
import { Product } from 'src/app/_models/product';
import { ActivatedRoute } from '@angular/router';
import { Category } from 'src/app/_models/category';
import { CategoryService } from 'src/app/_services/category.service';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { ProductService } from 'src/app/_services/product.service';
import { allResolved } from 'q';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {
  @ViewChild('editForm') editForm: NgForm;
  product: Product;
  categories: Category[];

  constructor(private route: ActivatedRoute, private categoryService: CategoryService,
    private alertify: AlertifyService, private productService: ProductService) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.product = data['product'];
    });
    this.loadCategories();
  }

  loadCategories() {
    this.categoryService.getCategories().subscribe((categories: Category[]) => {
      this.categories = categories;
    }, error => {
      this.alertify.error(error);
    });
  }

  updateProduct() {
    this.productService.putProduct(this.product.id, this.product).subscribe( next => {
      this.alertify.success('Product update successfully');
      this.editForm.reset(this.product);
    }, error => {
       this.alertify.error('Problem to update your product');
    });
    console.log(this.product);
  }
}
