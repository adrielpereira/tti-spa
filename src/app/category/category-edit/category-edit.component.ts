import { Component, OnInit, ViewChild } from '@angular/core';
import { Category } from 'src/app/_models/category';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { CategoryService } from 'src/app/_services/category.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-category-edit',
  templateUrl: './category-edit.component.html',
  styleUrls: ['./category-edit.component.css']
})
export class CategoryEditComponent implements OnInit {
  @ViewChild('editForm') editForm: NgForm;
  category: Category;

  constructor(private route: ActivatedRoute, private categoryService: CategoryService,
    private alertify: AlertifyService, private router: Router) { }

  ngOnInit() {
    this.route.data.subscribe(data => {
      this.category = data['category'];
    });
  }

  updateCategory() {
    this.categoryService.putCategory(this.category.id, this.category).subscribe( next => {
      this.alertify.success('Cateogry update successfully');
      this.editForm.reset(this.category);
      this.router.navigate(['/category']);
    }, error => {
       this.alertify.error('Problem to update your product');
    });
    console.log(this.category);
  }

  goBack() {
    this.router.navigate(['/category']);
  }
}
