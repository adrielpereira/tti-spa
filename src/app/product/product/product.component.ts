import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ProductService } from '../../_services/product.service';
import { CategoryService } from '../../_services/category.service';
import { Product } from 'src/app/_models/product';
import { AlertifyService } from 'src/app/_services/alertify.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  @Output() cancelAdd = new EventEmitter();
  categories: any;
  submitted = false;
  productForm: FormGroup;
  product: Product;

  constructor(private productService: ProductService, private categoryService: CategoryService, private alertify: AlertifyService,
     private formBuilder: FormBuilder, private route: Router) { }

  ngOnInit() {
    this.productForm = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(150)]),
      detail: new FormControl('', Validators.required),
      price: new FormControl('', Validators.required),
      categoryId: new FormControl('', Validators.required)
    });
    this.getCategory();
  }

  get f() { return this.productForm.controls; }

  onSubmit() {
    if (this.productForm.valid) {
      this.product = Object.assign({}, this.productForm.value);
      this.productService.postProduct(this.product).subscribe( () => {
        this.alertify.success('Product save successfully');
        this.route.navigate(['/home']);
      }, error => {
        this.alertify.error(error);
      });
    }
  }

  cancelAddProduct() {
    this.cancelAdd.emit(false);
  }

  getCategory() {
    this.categoryService.getCategories().subscribe( response => {
      this.categories = response;
    }, error => {
      console.log(error);
    });
  }

}
