import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { AlertifyService } from '../_services/alertify.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Subcategory } from '../_models/subcategory';
import { SubcategoryService } from '../_services/subcategory.service';


@Injectable()
export class SubcategoryDeleteResolver implements Resolve<Subcategory> {
    constructor( private subcategoryService: SubcategoryService, private route: Router, private alertify: AlertifyService) {}

    resolve(route: ActivatedRouteSnapshot): Observable<Subcategory> {
        return this.subcategoryService.getSubcategory(route.params['id']).pipe(
            catchError(error => {
                this.alertify.error(error);
                this.route.navigate(['/subcategory']);
                return of(null);
            })
        );
    }
}
